use clap::{Arg, App, ArgMatches};
use std::str::FromStr;
use errors::check_result;

pub enum Source {
    Forced(i8),
    Battery(String),
}

pub enum Command {
    ListBatteries,
    ShowStat { color: bool, capacity: bool, source: Option<Source> },
}

pub fn parse_args() -> Command {

    let cmd_list = Arg::with_name("list")
                       .short("l")
                       .long("list")
                       .help("Show detected batteries");

    let battery_name = Arg::with_name("battery_name")
                           .short("b")
                           .value_name("BAT")
                           .long("battery")
                           .help("Battery name to read data");

    let forced_value = Arg::with_name("forced_value")
                           .short("f")
                           .long("force-capacity")
                           .value_name("CAPACITY")
                           .help("Force a value");

    let only_color = Arg::with_name("only_color")
                         .short("o")
                         .long("only-color")
                         .help("Only write ANSI code to generate color");

    let only_capacity = Arg::with_name("only_capacity")
                            .short("c")
                            .long("only-capacity")
                            .help("Only write current capacity");

    let matches = App::new("catbat")
                      .version("0.1")
                      .arg(cmd_list)
                      .arg(battery_name)
                      .arg(forced_value)
                      .arg(only_color)
                      .arg(only_capacity)
                      .get_matches();

    if matches.is_present("list") {
        return Command::ListBatteries
    }

    // Extract arguments to show data

    let color = !matches.is_present("only_capacity");
    let capacity = !matches.is_present("only_color");
    let source = extract_source(&matches);

    Command::ShowStat { color: color, capacity: capacity, source: source }

}

fn extract_source(matches: &ArgMatches) -> Option<Source> {

    if let Some(forced) = matches.value_of("forced_value") {
        let forced = check_result("force argument", i8::from_str(forced));
        return Some(Source::Forced(forced));
    }

    if let Some(battery) = matches.value_of("battery_name") {
        return Some(Source::Battery(battery.into()));
    }

    None

}
