use std::io::stderr;
use std::io::Write;
use std::fmt::Debug;
use std::process::exit;

#[inline(always)]
pub fn check_result<T, E>(target: &str, result: Result<T, E>) -> T where E: Debug {

    let error = match result {
        Ok(x) => return x,
        Err(e) => e,
    };

    writeln!(stderr(), "Error on '{}': \x1b[31m{:?}\x1b[m", target, error).unwrap();
    exit(1);

}
