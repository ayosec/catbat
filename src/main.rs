extern crate clap;

mod batteries;
mod cli;
mod errors;

use std::cmp::min;
use cli::{ Command, Source };

fn main() {

    match cli::parse_args() {
        Command::ListBatteries => list_batteries(),
        Command::ShowStat { color, capacity, source } => show_stat(color, capacity, source),
    };

}

fn list_batteries() {
    for battery in batteries::get_all() {
        println!("{}", battery);
    }
}

fn show_stat(color: bool, capacity: bool, source: Option<Source>) {

    fn in_range(color: usize) -> usize { min(color / 10, 255) }

    for battery in batteries::from_source(&source) {
        if color {
            let factor = if battery.discharging { 10 } else { 13 };

            let (r, g, b) = match battery.capacity {
                c if c < 20 => (250, 75, 10),
                c => (90 + (100 - c) * 2, 235 - (100 - c) * 2, 10),
            };

            print!("\x1b[30;48;2;{};{};{}m", in_range(r * factor), in_range(g * factor), in_range(b));
        }
        if capacity {
            let dir = if battery.discharging { "▼" } else { "▲" };
            print!(" {} {}% ", dir, battery.capacity);
        }
    }
    if color && capacity {
        println!("\x1b[m");
    } else {
        print!("\n");
    }

}
