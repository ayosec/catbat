use cli::Source;
use errors::check_result;
use std::fmt;
use std::fs::{ DirEntry, File };
use std::io::{ self, Read };
use std::path::Path;
use std::str::FromStr;

const DATA_ROOT: &'static str = "/sys/class/power_supply";

pub struct Battery {
    pub name: String,
    pub capacity: usize,
    pub discharging: bool,
    pub manufacturer: Option<String>,
    pub model_name: Option<String>,
}

impl fmt::Display for Battery {

    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{} : {}% |", self.name, self.capacity)?;
        for x in &[ &self.manufacturer, &self.model_name ] {
            if let Some(ref x) = x.as_ref() {
                write!(fmt, " {}", x.trim())?;
            }
        }

        if self.discharging {
            fmt.write_str(" (discharging)")?;
        }

        Ok(())
    }

}

impl Battery {

    fn new(name: String,
           capacity: usize,
           discharging: bool,
           manufacturer: Option<String>,
           model_name: Option<String>) -> Battery {

        Battery {
            name: name,
            capacity: capacity,
            discharging: discharging,
            manufacturer: manufacturer,
            model_name: model_name,
        }

    }

}


pub fn get_all() -> Vec<Battery> {

    let entries = check_result(DATA_ROOT, Path::new(DATA_ROOT).read_dir());
    entries.filter_map(|x| x.ok())
           .filter(is_battery)
           .map(read_battery)
           .filter_map(|x| x.ok())
           .collect()

}

pub fn from_source(source: &Option<Source>) -> Vec<Battery> {
    match *source {
        None => get_all(),
        Some(Source::Forced(f)) => vec![Battery::new("FIX".into(), f.abs() as usize, f < 0, None, None)],
        Some(Source::Battery(ref bn)) => get_all().into_iter().filter(|b| b.name == *bn).collect(),
    }
}

fn is_battery(entry: &DirEntry) -> bool {

    match read_file(entry, "type") {
        Err(..) => false,
        Ok(d) => d.starts_with("Battery"),
    }

}

fn read_battery(entry: DirEntry) -> io::Result<Battery> {

    let name = entry.file_name().to_string_lossy().into();

    let capacity = usize::from_str(&read_file(&entry, "capacity")?.trim()).unwrap_or(0);

    let discharging = read_file(&entry, "status")?.starts_with("Discharging");

    let manufacturer = read_file(&entry, "manufacturer").ok();
    let model_name = read_file(&entry, "model_name").ok();

    Ok(Battery::new(name, capacity, discharging, manufacturer, model_name))

}

#[inline(always)]
fn read_file(entry: &DirEntry, filename: &str) -> io::Result<String> {
    let mut s = String::new();
    let mut parent = entry.path();
    parent.push(filename);
    File::open(parent)?.read_to_string(&mut s)?;
    Ok(s)
}
